﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YoutubeExplode;
using YoutubeExplode.Models.MediaStreams;

namespace Youtube_Downloader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private string videoID;

        private async void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string baseURL = "https://www.youtube.com/watch";

            if (txtURL.Text.StartsWith(baseURL))
            {
                videoID = YoutubeClient.ParseVideoId(txtURL.Text);

                await ExtractVideoInfoAsync();
            }
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            DownloadVideo();
        }

        private async Task ExtractVideoInfoAsync()
        {
            YoutubeClient infoClient = new YoutubeClient();

            //retrieve video info
            var video = await infoClient.GetVideoAsync(videoID);

            //Populate Video Details
            txtTitle.Text = video.Title;
            txtCreator.Text = video.Author;
            txtLength.Text = video.Duration.ToString();
        }

        private async void DownloadVideo()
        {
            YoutubeClient downloadClient = new YoutubeClient();
             
            //Retreieve best video audio info and audio file type
            var streamDetails = await downloadClient.GetVideoMediaStreamInfosAsync(videoID);
            var streamBestAudio = streamDetails.Audio.WithHighestBitrate();
            var fileExtension = streamBestAudio.Container.GetFileExtension();
            
            await downloadClient.DownloadMediaStreamAsync(streamBestAudio, $"downloaded_video.{fileExtension}");
        }
    }
}
